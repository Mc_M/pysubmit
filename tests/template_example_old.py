def render():

    # Possible import statements here

    # Settings
    START_SCRIPT_NAME = "template_test.var{}.sh"

    # Iterate N and steps_predict
    returns = []
    for var in [1, 2, 3, 4,]:

        # Output file name
        fname = START_SCRIPT_NAME.format(var)

        # Template variables
        context = {
            "var": var,
        }

        # Append data
        returns.append(
                        (fname, context)
                    )

    return returns

# Debugging
if __name__=="__main__":
    print("Debug render function ...")
    print("\t... print the content of render().")
    print()
    print(render())
