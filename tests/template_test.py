import shutil
import os

import pytest

from pysubmit.template import Template


# Test data
template_example_new_path = 'tests/template_example_new.py'
template_example_old_path = 'tests/template_example_old.py'
boilerplate_example_old_path = 'tests/template_example_old.boilerplate'
test_outdir = 'test_outdir'

def test_template_creation_newFormat():
    Template(template_example_new_path)

def test_template_creation_oldFormat():
    Template(template_example_old_path, boilerplate_path=boilerplate_example_old_path)

def test_template_render_me_newFormat():
    render_me_output = Template(template_example_new_path).render_me()
    output_desired = [
                        ('template_test.var1.sh', {'var': 1}),
                        ('template_test.var2.sh', {'var': 2}),
                        ('template_test.var3.sh', {'var': 3}),
                        ('template_test.var4.sh', {'var': 4})
                    ]
    for i, (fname_desired, context_desired) in enumerate(output_desired):
        fname, context = render_me_output[i]
        assert fname == fname_desired
        assert context == context_desired

def test_template_render_me_oldFormat():
    render_me_output = Template(template_example_old_path, boilerplate_example_old_path).render_me()
    output_desired = [
                        ('template_test.var1.sh', {'var': 1}),
                        ('template_test.var2.sh', {'var': 2}),
                        ('template_test.var3.sh', {'var': 3}),
                        ('template_test.var4.sh', {'var': 4})
                    ]
    for i, (fname_desired, context_desired) in enumerate(output_desired):
        fname, context = render_me_output[i]
        assert fname == fname_desired
        assert context == context_desired

def test_template_boilerplate_str_newFormat():
    template = Template(template_example_new_path)
    assert template.boilerplate_str == 'echo "This is the test template with var = {{var}}"\n'
    
def test_template_boilerplate_str_oldFormat():
    template = Template(template_example_old_path, boilerplate_path=boilerplate_example_old_path)
    assert template.boilerplate_str == 'echo "This is the test template with var = {{var}}"\n'

def test_template_create_files_newFormat():
    template = Template(template_example_new_path)
    # No start command
    files = template.create_files(test_outdir, start_command=None)
    for i in range(1, 5):
        content_desired = f'echo "This is the test template with var = {i}"'
        with open(os.path.join(test_outdir, f'template_test.var{i}.sh'), 'r') as f:
            content = f.readlines()
        content = ''.join(content)
        assert content == content_desired
    shutil.rmtree(test_outdir)
    # With start command
    template.create_files(test_outdir, start_command='start_me')
    for i in range(1, 5):
        content_desired = f'echo "This is the test template with var = {i}"'
        with open(os.path.join(test_outdir, f'template_test.var{i}.sh'), 'r') as f:
            content = f.readlines()
        content = ''.join(content)
        assert content == content_desired
    shutil.rmtree(test_outdir)
    
def test_template_create_files_oldFormat():
    template = Template(template_example_old_path, boilerplate_path=boilerplate_example_old_path)
    # No start command
    files = template.create_files(test_outdir, start_command=None)
    for i in range(1, 5):
        content_desired = f'echo "This is the test template with var = {i}"'
        with open(os.path.join(test_outdir, f'template_test.var{i}.sh'), 'r') as f:
            content = f.readlines()
        content = ''.join(content)
        assert content == content_desired
    shutil.rmtree(test_outdir)
    # With start command
    template.create_files(test_outdir, start_command='start_me')
    for i in range(1, 5):
        content_desired = f'echo "This is the test template with var = {i}"'
        with open(os.path.join(test_outdir, f'template_test.var{i}.sh'), 'r') as f:
            content = f.readlines()
        content = ''.join(content)
        assert content == content_desired
    shutil.rmtree(test_outdir)
