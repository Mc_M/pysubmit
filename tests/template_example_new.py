def render():
    """
    Needs to return a list of tuples. Each tuple contains the filename and the corresponding context values. The latter is a dictionary.


            Custom function that returns a list [x_1, x_2, ...] with
            x_i = (file name, context) a tuple holding a file name and
            a context. The *.template file will be rendered using the
            context - which is a directory providing keys for the
            *.template file - and saved with the file name.



        Loads the template's render() function.

        Output
        ------
        render() : function
            The template's render() function. It returns: [(fname, context)_i]_i with
            fname being the start script name and context the start script's context
            variables used in the template's boilerplate file.



    """

    # Possible import statements here

    # Settings
    START_SCRIPT_NAME = "template_test.var{}.sh"

    # Iterate N and steps_predict
    returns = []
    for var in [1, 2, 3, 4,]:

        # Output file name
        fname = START_SCRIPT_NAME.format(var)

        # Template variables
        context = {
            "var": var,
        }

        # Append data
        returns.append(
                        (fname, context)
                    )

    return returns

# Debugging
if __name__=="__main__":
    print("Debug render function ...")
    print("\t... print the content of render().")
    print()
    print(render())

"""<<<
echo "This is the test template with var = {{var}}"
>>>"""
